-- @file:       create_proc_insert_bundle.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

-- example: CALL sp_insert_bundle(1, "the new bundle", @bkey); SELECT @bkey AS bundle_key;

DELIMITER //

CREATE PROCEDURE sp_insert_bundle (
  IN arg_location_key INT UNSIGNED, 
  IN arg_bundle_name VARCHAR(128), 
  OUT arg_bundle_key INT UNSIGNED
)
  BEGIN
  
  -- begin transaction
  START TRANSACTION;
  
  -- INSERT bundle
  INSERT INTO bundle (location_key, bundle_name) 
    VALUES (arg_location_key, arg_bundle_name);
  
  -- GET bundle_key
  SELECT LAST_INSERT_ID() INTO arg_bundle_key;
  
  -- GET supply_status_key
  SELECT supply_status_key FROM supply_status 
    WHERE procedure_name = 'sp_insert_bundle' INTO @arg_supply_status_key;
    
  IF arg_bundle_key IS NOT NULL THEN
    -- INSERT bundle_status
    INSERT INTO bundle_status (supply_status_key, bundle_key) 
      VALUES (@arg_supply_status_key, arg_bundle_key);
    -- PASS: commit transaction
    COMMIT;
  ELSE 
    -- FAIL: rollback transaction
    ROLLBACK;
  END IF;
END;
//

DELIMITER ;