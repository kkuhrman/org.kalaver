-- @file:       create_table_supply_status.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

CREATE TABLE IF NOT EXISTS supply_status (
  supply_status_key INT UNSIGNED PRIMARY KEY,
  supply_status_name VARCHAR(128) NOT NULL,
  procedure_name VARCHAR(128) NOT NULL,
CONSTRAINT `unique_procedure_name` UNIQUE INDEX
    `idx_procedure_name` (procedure_name)
) ENGINE = InnoDB;

-- ALTER TABLE supply_status 
--  ADD COLUMN procedure_name VARCHAR(128) NOT NULL AFTER supply_status_name, 
--  ADD CONSTRAINT `unique_procedure_name` UNIQUE INDEX `idx_procedure_name` (procedure_name);