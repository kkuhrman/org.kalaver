-- @file:       create_table_location.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

CREATE TABLE IF NOT EXISTS location (
  location_key INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  location_name VARCHAR(128) NOT NULL
) ENGINE = InnoDB;