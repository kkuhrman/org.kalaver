-- @file:       create_table_bundle_status.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

CREATE TABLE IF NOT EXISTS bundle_status (
  supply_status_key INT UNSIGNED NOT NULL,  
  bundle_key INT UNSIGNED NOT NULL,
  dtm_change TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(2),
 CONSTRAINT `fk_bundle_status_1` FOREIGN KEY
    `idx_bundle_status_1` (supply_status_key)
    REFERENCES supply_status (supply_status_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 CONSTRAINT `fk_bundle_status_2` FOREIGN KEY
    `idx_bundle_status_2` (bundle_key)
    REFERENCES bundle (bundle_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
 ) ENGINE = InnoDB;
 
  
  