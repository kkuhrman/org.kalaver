-- @file:       create_table_bundle.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

CREATE TABLE IF NOT EXISTS bundle (
  bundle_key INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  location_key INT UNSIGNED NOT NULL,
  bundle_name VARCHAR(128) NOT NULL DEFAULT 'My bundle',
 CONSTRAINT `fk_location_key` FOREIGN KEY
    `idx_bundle_location` (location_key)
    REFERENCES location (location_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
 ) ENGINE = InnoDB;
 
  
  