-- @file:       create_table_bundle_item.sql
-- @brief:      part of schema 1.0.0-DEV
-- @authors:    Karl Kuhrman
-- @license:    GPL3 (@see COPYING)
-- @copyright:  2020 Kuhrman Technology Solutions LLC
-- Report bugs to bugs@kalaver.org

CREATE TABLE IF NOT EXISTS bundle_item (
  bundle_item_key INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  bundle_key INT UNSIGNED,
  product_key INT UNSIGNED NOT NULL,
  uom_key INT UNSIGNED NOT NULL,
  line_item_number INT UNSIGNED NOT NULL,
  quantity INT UNSIGNED NOT NULL,
 CONSTRAINT `fk_bundle_item_1` FOREIGN KEY
    `idx_bundle_item_1` (bundle_key)
    REFERENCES bundle (bundle_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_bundle_item_2` FOREIGN KEY
    `idx_bundle_item_2` (product_key)
    REFERENCES product (product_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_bundle_item_3` FOREIGN KEY
    `idx_bundle_item_3` (uom_key)
    REFERENCES uom (uom_key)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
 ) ENGINE = InnoDB;
 
  
  