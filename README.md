# README #

Kalaver is a Free Software inventory control system written in C. 


### kalaver.org project ###

Kalavercomprises multiple daemons (services) which communicate with one another 
by way of pipes or TCP sockets or asynchronously by way of REST web services.

This repository comprises the kalaver.org daemons and at least one client.

Kalaver is a portmanteau of kaufen, lagern and verkaufen - the German verbs for buy, store, sell respectively.
 
### building kalaver ###

See INSTALL in the root directory of the project

### communications ###

Kalaver software is created and maintained by Kuhrman Technology Solutions LLC.

Direct any questions about this project to Kuhrman Technology Solutions LLC info@kuhrman.com.

Defects should be reported to bugs@kalaver.org.